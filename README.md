# Using docker image

- docker pull registry.gitlab.com/androiddockerci/allandroiddockerci:master
- docker images
- docker run -i -t --entrypoint /bin/bash  **< image id >**

# Cleaning up docker

- docker rm $(docker ps -q -f 'status=exited')   
- docker rmi $(docker images -q -f "dangling=true")  
- docker kill $(docker ps -q)  